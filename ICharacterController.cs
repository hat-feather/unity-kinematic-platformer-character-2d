﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    public interface ICharacterController
    {
        void UpdateVelocity(ref Vector2 currentVelocity, float deltaTime);

        void PostSimulationUpdate(float deltaTime);
    }
}
