
namespace HatFeather.KinematicPlatformerCharacter2D
{
    public enum MotorUpdateMethod
    {
        EveryFrame,
        EveryFixedUpdate,
    }
}