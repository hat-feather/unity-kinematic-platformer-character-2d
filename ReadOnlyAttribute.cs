﻿using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    public class ReadOnlyAttribute : PropertyAttribute
    {
    }
}