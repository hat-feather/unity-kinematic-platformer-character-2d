# Unity Kinematic Platformer Character 2D

`Unity Kinematic Platformer Character 2D` is a package built for Unity that provides a kinematic platformer character which handles 2D collisions.

## Dependencies

* None

## How to Use

* Create a character controller class (i.e. MyCharacterController.cs") which inherits from MonoBehaviour
* Implement the ICharacterController interface (on your character controller class)
* In the awake method on your character controller, reference the motor and assign the character controller
* Use the methods provided by the ICharacterController interface to implement your character functionality
```
void Awake()
{
	GetComponent<KinematicCharacterMotor2d>().CharacterController = this;
}
```

## Known Limitations

* Collisions with corners are a little jagged
* The box shape is not yet supported
* The rotation is locked at 0 degrees
