﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    public sealed class KinematicCharacterMotor2D : MonoBehaviour
    {
        public const int HitBufferSize = 16;
        public const float LockedRotation = 0.0f;

        [SerializeField] private MotorDimensions dimensions = new MotorDimensions();
        [SerializeField] private MotorSolverPrefs solverPrefs = new MotorSolverPrefs();

        private GroundInformation groundInfo = new GroundInformation();
        private GroundInformation lastGroundInfo = new GroundInformation();
        private WallInformation wallInfo = new WallInformation();
        private WallInformation lastWallInfo = new WallInformation();
        private SimulatedMotorInformation simInfo = new SimulatedMotorInformation();
        private SimulatedMotorInformation lastSimInfo = new SimulatedMotorInformation();
        private ICharacterController characterController = null;
        private RaycastHit2D[] hitBuffer = new RaycastHit2D[HitBufferSize];
        private bool shouldSimulate = true;

        #region callbacks and events

        public event Action<GroundInformation> GroundInfoUpdated = null;
        public event Action<WallInformation> WallInfoUpdated = null;
        public event Action SimulationStarted = null;
        public event Action SimulationFinished = null;

        #endregion

        #region properties

        public MotorDimensions Dimensions => dimensions;
        public MotorSolverPrefs SolverPrefs => solverPrefs;
        public GroundInformation GroundInfo => groundInfo;
        public GroundInformation LastGroundInfo => lastGroundInfo;
        public WallInformation WallInfo => wallInfo;
        public WallInformation LastWallInfo => lastWallInfo;
        public SimulatedMotorInformation SimulatedInfo => simInfo;
        public SimulatedMotorInformation LastSimulatedInfo => lastSimInfo;

        public bool ShouldSimulate
        {
            get => shouldSimulate;
            set
            {
                bool prev = shouldSimulate;
                shouldSimulate = value;

                if (!prev && shouldSimulate)
                    ResetSimulatedInfo();
            }
        }

        public ICharacterController CharacterController
        {
            get => characterController;
            set => characterController = value;
        }

        #endregion

        private void Awake()
        {
            ValidateData();
        }

        private void Start()
        {
            ResetSimulatedInfo();
        }

        private void LateUpdate()
        {
            if (solverPrefs.UpdateMethod == MotorUpdateMethod.EveryFrame)
                Simulate(Time.deltaTime);
        }

        private void FixedUpdate()
        {
            if (solverPrefs.UpdateMethod == MotorUpdateMethod.EveryFixedUpdate)
                Simulate(Time.fixedDeltaTime);
        }

        private void Simulate(float deltaTime)
        {
            if (!shouldSimulate)
                return;

            if (SimulationStarted != null)
                SimulationStarted();

            Vector2 position = simInfo.position;
            Move(ref position, deltaTime);
            PreventOverlaps(ref position);

            UpdateSimulatedInfo(position, LockedRotation, deltaTime);
            SyncTransform();

            UpdateGroundInfo();
            UpdateWallInfo();

            if (SimulationFinished != null)
                SimulationFinished();

            characterController.PostSimulationUpdate(deltaTime);
        }

        private void UpdateWallInfo()
        {
            bool didHit;
            int hitIndex;
            int hitCount;

            Vector2 castSize = new Vector2(dimensions.InnerSize.x / 2, dimensions.InnerSize.y - dimensions.InnerSize.x);
            float castDistance = castSize.x / 2 + dimensions.ShellSize + solverPrefs.WallCheckDistance;

            // Check for a wall on the left first
            hitCount = Physics2D.BoxCast(simInfo.position, castSize, LockedRotation, Vector2.left,
                solverPrefs.CollisionFilter, hitBuffer, castDistance);
            didHit = GetHitBufferCollisionIndex(hitCount, out hitIndex);

            // Check for a wall on the right if there as no wall on the left
            if (!didHit)
            {
                hitCount = Physics2D.BoxCast(simInfo.position, castSize, LockedRotation, Vector2.right,
                    solverPrefs.CollisionFilter, hitBuffer, castDistance);
                didHit = GetHitBufferCollisionIndex(hitCount, out hitIndex);
            }

            // Update wall information
            lastWallInfo = wallInfo;
            if (didHit)
            {
                RaycastHit2D hit = hitBuffer[hitIndex];

                wallInfo.contact = hit.point;
                wallInfo.normal = hit.normal;
                wallInfo.exists = true;
            }
            else
            {
                wallInfo.contact = Vector2.zero;
                wallInfo.normal = Vector2.zero;
                wallInfo.exists = false;
            }

            if (WallInfoUpdated != null)
                WallInfoUpdated(wallInfo);
        }

        private void UpdateGroundInfo()
        {
            float castDist = dimensions.ShellSize + solverPrefs.GroundCheckDistance;
            int hitCount = BodyCollisionCast(simInfo.position, dimensions.InnerSize, Vector2.down, castDist);

            lastGroundInfo = groundInfo;

            int index;
            if (GetHitBufferCollisionIndex(hitCount, out index))
            {
                RaycastHit2D hit = hitBuffer[index];

                groundInfo.contact = hit.point;
                groundInfo.foundAny = true;
                groundInfo.normal = hit.normal;
                groundInfo.slope = Vector2.Angle(Vector2.up, hit.normal);
                groundInfo.isStable = groundInfo.slope < solverPrefs.MaxStableGroundAngle;
            }
            else
            {
                groundInfo.contact = Vector2.zero;
                groundInfo.foundAny = false;
                groundInfo.isStable = false;
                groundInfo.normal = Vector2.zero;
                groundInfo.slope = 0.0f;
            }

            if (GroundInfoUpdated != null)
                GroundInfoUpdated(groundInfo);
        }

        private void PreventOverlaps(ref Vector2 position)
        {
            for (int i = 0; i < solverPrefs.OverlapSolverIterations; ++i)
            {
                if (!SolveOverlap(ref position))
                    break;
            }
        }

        private bool SolveOverlap(ref Vector2 currentPos)
        {
            int hitCount = BodyCollisionCast(currentPos, dimensions.OuterSize, Vector2.zero, 0.0f);

            int index;
            if (GetHitBufferCollisionIndex(hitCount, out index))
            {
                RaycastHit2D hit = hitBuffer[index];

                Vector2 a = ClosestPointOnCapsule(currentPos, dimensions.OuterSize, hit.point);
                Vector2 b = hit.collider.ClosestPoint(hit.point);

                currentPos += b - a;

                return true;
            }

            return false;
        }

        private void Move(ref Vector2 position, float deltaTime)
        {
            Vector2 movement = GetTargetMovementDelta(deltaTime);
            Vector2 direction = movement.normalized;
            float distance = movement.magnitude;

            for (int i = 0; i < solverPrefs.MovementSolverIterations; ++i)
            {
                if (Mathf.Approximately(distance, 0.0f))
                    break;

                SolveMove(ref position, ref direction, ref distance);
            }
        }

        private void SolveMove(ref Vector2 currentPos, ref Vector2 moveDir, ref float remainingDist)
        {
            float castDist = Mathf.Max(remainingDist, dimensions.ShellSize * 2);
            int hitCount = BodyCollisionCast(currentPos, dimensions.InnerSize, moveDir, castDist);

            int index;
            if (GetHitBufferCollisionIndex(hitCount, out index))
            {
                RaycastHit2D hit = hitBuffer[index];

                // Move to the first wall hit
                // TODO don't allow move to go past wall!
                float dot = Vector2.Dot((hit.point - currentPos).normalized, moveDir);
                float moveDist = hit.distance * dot - dimensions.ShellSize;
                moveDist = Mathf.Clamp(moveDist, 0.0f, remainingDist);
                remainingDist -= moveDist;
                currentPos = currentPos + moveDir * moveDist;

                // Collide and slide
                Vector2 slideDir = Vector3.Cross(hit.normal, Vector3.Cross(moveDir, hit.normal)).normalized;
                remainingDist = Vector2.Dot(moveDir, slideDir) * remainingDist;
                moveDir = slideDir;
            }
            else
            {
                // No collisions found, safely move
                currentPos += moveDir * remainingDist;
                remainingDist = 0.0f;
            }
        }

        private int BodyCollisionCast(Vector2 origin, Vector2 size, Vector2 direction, float distance)
        {
            switch (dimensions.Shape)
            {
                case MotorShape.Capsule:
                    return Physics2D.CapsuleCast(origin, size, CapsuleDirection2D.Vertical, LockedRotation, direction,
                        solverPrefs.CollisionFilter, hitBuffer, distance);
                case MotorShape.Box:
                    return Physics2D.BoxCast(origin, size, LockedRotation, direction, solverPrefs.CollisionFilter,
                        hitBuffer, distance);
                default:
                    throw new System.NotImplementedException(dimensions.Shape.ToString());
            }
        }

        private void UpdateSimulatedInfo(Vector2 position, float rotation, float deltaTime)
        {
            lastSimInfo = simInfo;

            simInfo.position = position;
            simInfo.rotation = LockedRotation;
            simInfo.velocity = (position - lastSimInfo.position) / deltaTime;
        }

        private void ResetSimulatedInfo()
        {
            simInfo.position = transform.position;
            simInfo.rotation = transform.eulerAngles.z;
            simInfo.velocity = Vector2.zero;

            lastSimInfo = simInfo;
        }

        private void SyncTransform()
        {
            transform.position = simInfo.position;
            transform.rotation = GetQuaternion(simInfo.rotation);
        }

        private Vector2 GetTargetMovementDelta(float deltaTime)
        {
            Vector2 result = simInfo.velocity;

            characterController.UpdateVelocity(ref result, deltaTime);

            result *= deltaTime;
            return result;
        }

        private bool GetHitBufferCollisionIndex(int hitCount, out int index)
        {
            for (int i = 0; i < hitCount; ++i)
            {
                if (!IsValidForCollisions(hitBuffer[i].collider))
                    continue;

                index = i;
                return true;
            }

            index = -1;
            return false;
        }

        private bool IsValidForCollisions(Collider2D collider)
        {
            return collider.transform != this.transform && !collider.isTrigger;
        }

        private Vector2 ClosestPointOnCapsule(Vector2 capsPos, Vector2 capsSize, Vector2 point)
        {
            Vector2 localResult;

            Vector2 localPoint = point - capsPos;
            float radius = capsSize.x / 2;
            float cylHalfHeight = capsSize.y / 2 - radius;

            if (localPoint.y < -cylHalfHeight || localPoint.y > cylHalfHeight)
            {
                float sign = Mathf.Sign(localPoint.y);
                Vector2 origin = new Vector2(0.0f, sign * cylHalfHeight);

                float rads = Vector2.SignedAngle(localPoint - origin, Vector2.up) * Mathf.Deg2Rad;
                float x = Mathf.Sin(rads) * radius;
                float y = Mathf.Cos(rads) * radius;

                localResult = new Vector2(x, y + sign * cylHalfHeight);
            }
            else
                localResult = new Vector2(localPoint.x > 0 ? radius : -radius, localPoint.y);

            return localResult + capsPos;
        }

        private Vector2 ClosestPointOnBox(Vector2 boxPos, Vector2 boxSize, Vector2 point)
        {
            throw new NotImplementedException();
        }

        private Vector2 RotateCounterclockwise(Vector2 vector, float degrees)
        {
            return RotateClockwise(vector, -degrees);
        }

        private Vector2 RotateClockwise(Vector2 vector, float degrees)
        {
            float radians = Mathf.Deg2Rad * degrees;
            float x = Mathf.Cos(radians) * vector.x + Mathf.Sin(radians) * vector.y;
            float y = -Mathf.Sin(radians) * vector.x + Mathf.Cos(radians) * vector.y;

            return new Vector2(x, y);
        }

        private Quaternion GetQuaternion(float angle) => Quaternion.Euler(0, 0, angle);

        #region validiation

        private void Reset()
        {
            ValidateData();
        }

        private void OnValidate()
        {
            ValidateData();
        }

        private void ValidateData()
        {
            solverPrefs.ValidateData(this);
            dimensions.ValidateData(this);
        }

        #endregion
    }
}
