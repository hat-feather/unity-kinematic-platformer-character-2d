using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    [System.Serializable]
    public class MotorSolverPrefs : IValidatable
    {
        public const int MinMoveIterations = 1;
        public const int MaxMoveIterations = 16;
        public const int MinOverlapIterations = 0;
        public const int MaxOverlapIterations = 8;

        [SerializeField] private MotorUpdateMethod updateMethod = MotorUpdateMethod.EveryFrame;
        [Space]
        [SerializeField] private LayerMask collisionMask = Physics.AllLayers;
        [SerializeField] private float wallRange = 0.05f;
        [SerializeField] private float groundRange = 0.05f;
        [Space]
        [SerializeField, Range(0, 89)] private float maxGroundAngle = 45.0f;
        [Space]
        [SerializeField] private int moveIterations = 6;
        [SerializeField] private int overlapIterations = 4;
        [Space]
        [SerializeField, ReadOnly] private ContactFilter2D collisionFilter = new ContactFilter2D();

        public MotorUpdateMethod UpdateMethod => updateMethod;

        public ContactFilter2D CollisionFilter => collisionFilter;
        public LayerMask CollisionMask => collisionMask;
        public float WallCheckDistance => wallRange;
        public float GroundCheckDistance => groundRange;

        public float MaxStableGroundAngle => maxGroundAngle;

        public int MovementSolverIterations => moveIterations;
        public int OverlapSolverIterations => overlapIterations;

        public void ValidateData(KinematicCharacterMotor2D motor)
        {
            collisionFilter.SetLayerMask(collisionMask);
            collisionFilter.useLayerMask = true;
            collisionFilter.useTriggers = false;

            wallRange = Mathf.Clamp(wallRange, 0.0f, float.MaxValue);
            groundRange = Mathf.Clamp(groundRange, 0.0f, float.MaxValue);

            moveIterations = Mathf.Clamp(moveIterations, MinMoveIterations, MaxMoveIterations);
            overlapIterations = Mathf.Clamp(overlapIterations, MinOverlapIterations, MaxOverlapIterations);
        }
    }
}
