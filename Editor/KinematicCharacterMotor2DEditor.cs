﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(KinematicCharacterMotor2D))]
    internal class KinematicCharacterMotor2DEditor : Editor
    {
        private static readonly Color InnerCapsuleColor = new Color(1.0f, 0.92f, 0.016f);
        private static readonly Color OuterCapsuleColor = new Color(0.0f, 1.0f, 0.0f);
        private static readonly Color CheckColor = new Color(1.0f, 0.0f, 1.0f);

        private static Color prevGizmosColor;
        private static Matrix4x4 prevGizmosMatrix;
        private static Color prevHandlesColor;
        private static Matrix4x4 prevHandlesMatrix;

        public override void OnInspectorGUI()
        {
            KinematicCharacterMotor2D motor = target as KinematicCharacterMotor2D;

            base.OnInspectorGUI();

            EditorGUILayout.HelpBox(
                "The inner shell performs a cast to check for collisions, while the outer " +
                "shell is used to resolve overlaps. Keep this in mind as you develop your game.",
                MessageType.Info
            );

            if (motor.Dimensions.Shape == MotorShape.Box)
            {
                EditorGUILayout.HelpBox(
                    "The box shape is not currently supported!",
                    MessageType.Error
                );
            }

            if (Application.isPlaying)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Playmode Actions ", EditorStyles.boldLabel);
                motor.ShouldSimulate = EditorGUILayout.Toggle("Should Simulate", motor.ShouldSimulate);
            }
        }

        [DrawGizmo(GizmoType.Selected | GizmoType.Active)]
        private static void DrawMotorGizmos(KinematicCharacterMotor2D motor, GizmoType gizmoType)
        {
            if (motor.Dimensions.Shape == MotorShape.Capsule)
            {
                DrawCapsuleGizmo(motor.transform, motor.Dimensions.InnerSize, GetGizmoColor(InnerCapsuleColor, gizmoType));
                DrawCapsuleGizmo(motor.transform, motor.Dimensions.OuterSize, GetGizmoColor(OuterCapsuleColor, gizmoType));
            }
            else if (motor.Dimensions.Shape == MotorShape.Box)
            {
                DrawBoxGizmo(motor.transform, motor.Dimensions.InnerSize, GetGizmoColor(InnerCapsuleColor, gizmoType));
                DrawBoxGizmo(motor.transform, motor.Dimensions.OuterSize, GetGizmoColor(OuterCapsuleColor, gizmoType));
            }
            else
                throw new System.NotImplementedException(motor.Dimensions.Shape.ToString());

            DrawWallCheckGizmos(motor, gizmoType);
            DrawGroundCheckGizmos(motor, gizmoType);
        }

        private static Color GetGizmoColor(Color source, GizmoType gizmoType) => source;

        private static void DrawGroundCheckGizmos(KinematicCharacterMotor2D motor, GizmoType gizmoType)
        {
            StorePrevGizmos();

            float checkDist = motor.SolverPrefs.GroundCheckDistance;
            float xOffset = motor.Dimensions.InnerSize.x / 5;
            float yOffset = -motor.Dimensions.OuterSize.y / 2 - checkDist;
            Vector2 a = new Vector2(-xOffset, yOffset);
            Vector2 b = new Vector2(xOffset, yOffset);

            Gizmos.color = Gizmos.color = GetGizmoColor(CheckColor, gizmoType);
            Gizmos.matrix = motor.transform.localToWorldMatrix;
            Gizmos.DrawLine(a, b);

            RestorePrevGizmos();
        }

        private static void DrawWallCheckGizmos(KinematicCharacterMotor2D motor, GizmoType gizmoType)
        {
            StorePrevGizmos();

            float z = motor.transform.position.z;
            float xOffset = motor.Dimensions.OuterSize.x / 2 + motor.SolverPrefs.WallCheckDistance;
            float yOffset = motor.Dimensions.InnerSize.y / 2 - motor.Dimensions.InnerSize.x / 2;

            Vector3 topL = new Vector3(-xOffset, yOffset, z);
            Vector3 topR = new Vector3(xOffset, yOffset, z);
            Vector3 botL = new Vector3(-xOffset, -yOffset, z);
            Vector3 botR = new Vector3(xOffset, -yOffset, z);

            Gizmos.matrix = motor.transform.localToWorldMatrix;
            Gizmos.color = GetGizmoColor(CheckColor, gizmoType);

            Gizmos.DrawLine(topL, botL);
            Gizmos.DrawLine(topR, botR);

            RestorePrevGizmos();
        }

        private static void DrawBoxGizmo(Transform transform, Vector2 size, Color color)
        {
            StorePrevGizmos();

            Matrix4x4 matrix = transform.localToWorldMatrix;
            Gizmos.color = color;
            Gizmos.matrix = matrix;
            Handles.color = color;
            Handles.matrix = matrix;

            float z = transform.position.z;
            float xOffset = size.x / 2;
            float yOffset = size.y / 2;

            Vector3 topL = new Vector3(-xOffset, yOffset, z);
            Vector3 topR = new Vector3(xOffset, yOffset, z);
            Vector3 botL = new Vector3(-xOffset, -yOffset, z);
            Vector3 botR = new Vector3(xOffset, -yOffset, z);

            Gizmos.DrawLine(topL, topR);
            Gizmos.DrawLine(topR, botR);
            Gizmos.DrawLine(botR, botL);
            Gizmos.DrawLine(botL, topL);

            RestorePrevGizmos();
        }

        private static void DrawCapsuleGizmo(Transform transform, Vector2 size, Color color)
        {
            StorePrevGizmos();

            Matrix4x4 matrix = transform.localToWorldMatrix;
            Gizmos.color = color;
            Gizmos.matrix = matrix;
            Handles.color = color;
            Handles.matrix = matrix;

            float z = transform.position.z;
            float xOffset = size.x / 2;
            float yOffset = size.y / 2 - xOffset;

            Vector3 topL = new Vector3(-xOffset, yOffset, z);
            Vector3 topR = new Vector3(xOffset, yOffset, z);
            Vector3 botL = new Vector3(-xOffset, -yOffset, z);
            Vector3 botR = new Vector3(xOffset, -yOffset, z);

            Gizmos.DrawLine(topL, botL);
            Gizmos.DrawLine(topR, botR);

            Vector3 topCenter = (topL + topR) / 2;
            Vector3 botCenter = (botL + botR) / 2;

            Handles.DrawWireArc(topCenter, Vector3.forward, Vector3.right, 180, xOffset);
            Handles.DrawWireArc(botCenter, Vector3.forward, Vector3.left, 180, xOffset);

            RestorePrevGizmos();
        }

        private static void StorePrevGizmos()
        {
            prevGizmosColor = Gizmos.color;
            prevGizmosMatrix = Gizmos.matrix;
            prevHandlesColor = Handles.color;
            prevHandlesMatrix = Handles.matrix;
        }

        private static void RestorePrevGizmos()
        {
            Gizmos.color = prevGizmosColor;
            Gizmos.matrix = prevGizmosMatrix;
            Handles.color = prevHandlesColor;
            Handles.matrix = prevHandlesMatrix;
        }
    }
}