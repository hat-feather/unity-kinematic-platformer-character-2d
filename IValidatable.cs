using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    public interface IValidatable
    {
        void ValidateData(KinematicCharacterMotor2D motor);
    }
}
