﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    public enum MotorShape
    {
        Capsule,
        Box,
    }
}
