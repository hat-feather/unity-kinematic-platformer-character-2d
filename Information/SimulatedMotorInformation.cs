﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    public struct SimulatedMotorInformation
    {
        public Vector2 position;
        public float rotation;
        public Vector2 velocity;
    }
}
