﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    public struct WallInformation
    {
        public bool exists;
        public Vector2 contact;
        public Vector2 normal;
    }
}
