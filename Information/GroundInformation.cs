﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    public struct GroundInformation
    {
        public bool isStable;
        public bool foundAny;
        public float slope;
        public Vector2 contact;
        public Vector2 normal;
    }
}
