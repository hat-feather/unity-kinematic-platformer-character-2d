using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HatFeather.KinematicPlatformerCharacter2D
{
    [System.Serializable]
    public class MotorDimensions : IValidatable
    {
        [SerializeField] private MotorShape shape = MotorShape.Capsule;
        [Space]
        [SerializeField] private float width = 0.5f;
        [SerializeField] private float height = 1.0f;
        [SerializeField] private float shellSize = 0.02f;
        [Space]
        [SerializeField, ReadOnly] private Vector2 innerSize = new Vector2();
        [SerializeField, ReadOnly] private Vector2 outerSize = new Vector2();

        public MotorShape Shape => shape;
        public Vector2 InnerSize => innerSize;
        public Vector2 OuterSize => outerSize;
        public float OuterWidth => width;
        public float OuterHeight => height;
        public float ShellSize => shellSize;

        public void SetOuterSize(Vector2 size) => SetOuterSize(size.x, size.y);

        public void SetOuterSize(float width, float height)
        {
            float totalShell = shellSize * 2;

            this.width = Mathf.Clamp(width, totalShell, float.MaxValue);
            this.height = Mathf.Clamp(Mathf.Max(this.width, height), totalShell, float.MaxValue);

            outerSize.x = this.width;
            outerSize.y = this.height;

            innerSize.x = this.width - totalShell;
            innerSize.y = this.height - totalShell;
        }

        public void ValidateData(KinematicCharacterMotor2D motor)
        {
            shellSize = Mathf.Clamp(shellSize, 0.0f, width / 2);
            SetOuterSize(width, height);
        }
    }
}
